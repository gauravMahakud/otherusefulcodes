#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 23:41:46 2018

@author: gaurav.mahakud@jumbotail.com
"""

import re
import csv
import gspread
from oauth2client.service_account import ServiceAccountCredentials


#copy paste the mail in failStr below
#
failStr = """
Look into the failure. System Will NOT Retry Anymore !!!
Request : BoltOrderItem(orderItemId=BLTORDITM-1318492181, orderId=BLTORD-1311817156, buyerId=JC-1202512507, sellerId=ORGPROF-1304473270, listingId=LST-1209530076, boltOrderType=MARKETPLACE, rpoReasonCode=null, quantity=144, mrp=2, taxMaster=TaxMaster(taxMasterID=TAXM-1304473371, jpin=JPIN-1304472092, hsnCode=21011120, sinTax=null, cess=null, vatPercentage=5.5, gstPercentage=18.0, gstComponentShare={CGST=50.0, SGST=50.0}, validityPeriodStartDate=null, validityPeriodEndDate=null), vat=5.5, sellingPrice=1.75, orderItemAmount=252, shippingCharges=0.07200000000000001, createdUnitQuantity=144, confirmedUnitQuantity=0, underProcessUnitQuantity=0, readyToShipUnitQuantity=0, shippedUnitQuantity=0, inTransitUnitQuantity=0, deliveredUnitQuantity=0, returnedUnitQuantity=0, cancelledUnitQuantity=0, returnRequestedQuantity=0, returnToOriginQuantity=0, rtsTime=null, orderItemStatus=Created, orderItemStatusRemark=null, invoiceNumbers=null, orderPlacedTimestamp=1567667620085, tradeOffer=null, offers=[], jtOfferBOList=null, appliedJtOfferIds=[], cartItemID=CRTITM-1307885072, invoicedQuantity=0, invoiceItemIdToQtyMap=null, replaceeOrderItemId=BLTORDITM-1318492187, isReplacement=false, returnedOrderItemId=null, statusUpdateHistory=null, mpCommission=MpCommissionBO(entityId=SP-1388879426, entityType=SELLER_PRODUCT, mpCommissionType=PERCENTAGE, value=0.1), rmcTaxPercentage=null)
Exception : null
StackTrace :
-- 
You received this message because you are subscribed to the Google Groups "tech" group.
To unsubscribe from this group and stop receiving emails from it, send an email to tech+unsubscribe@jumbotail.com.
To view this discussion on the web visit https://groups.google.com/a/jumbotail.com/d/msgid/tech/0102016d00459eb3-af2a825e-6759-4e31-8237-b26ec1589c3f-000000%40eu-west-1.amazonses.com.
Look into the failure. System Will NOT Retry Anymore !!!
Request : BoltOrderItem(orderItemId=BLTORDITM-1318492037, orderId=BLTORD-1311817314, buyerId=JC-1202540539, sellerId=ORGPROF-1304473270, listingId=LST-1209530076, boltOrderType=MARKETPLACE, rpoReasonCode=null, quantity=144, mrp=2, taxMaster=TaxMaster(taxMasterID=TAXM-1304473371, jpin=JPIN-1304472092, hsnCode=21011120, sinTax=null, cess=null, vatPercentage=5.5, gstPercentage=18.0, gstComponentShare={CGST=50.0, SGST=50.0}, validityPeriodStartDate=null, validityPeriodEndDate=null), vat=5.5, sellingPrice=1.75, orderItemAmount=252, shippingCharges=0.07200000000000001, createdUnitQuantity=144, confirmedUnitQuantity=0, underProcessUnitQuantity=0, readyToShipUnitQuantity=0, shippedUnitQuantity=0, inTransitUnitQuantity=0, deliveredUnitQuantity=0, returnedUnitQuantity=0, cancelledUnitQuantity=0, returnRequestedQuantity=0, returnToOriginQuantity=0, rtsTime=null, orderItemStatus=Created, orderItemStatusRemark=null, invoiceNumbers=null, orderPlacedTimestamp=1567667381091, tradeOffer=null, offers=[], jtOfferBOList=null, appliedJtOfferIds=[], cartItemID=CRTITM-1307885474, invoicedQuantity=0, invoiceItemIdToQtyMap=null, replaceeOrderItemId=BLTORDITM-1318492040, isReplacement=false, returnedOrderItemId=null, statusUpdateHistory=null, mpCommission=MpCommissionBO(entityId=SP-1388879426, entityType=SELLER_PRODUCT, mpCommissionType=PERCENTAGE, value=0.1), rmcTaxPercentage=null)
Exception : null
StackTrace :
-- 
You received this message because you are subscribed to the Google Groups "tech" group.
To unsubscribe from this group and stop receiving emails from it, send an email to tech+unsubscribe@jumbotail.com.
To view this discussion on the web visit https://groups.google.com/a/jumbotail.com/d/msgid/tech/0102016d004250b6-21517f81-4be5-4cd2-8b7e-95f54b138f6a-000000%40eu-west-1.amazonses.com.
Look into the failure. System Will NOT Retry Anymore !!!
Request : BoltOrderItem(orderItemId=BLTORDITM-1318492006, orderId=BLTORD-1311817436, buyerId=JC-1202549913, sellerId=ORGPROF-1304473270, listingId=LST-1209530076, boltOrderType=MARKETPLACE, rpoReasonCode=null, quantity=288, mrp=2, taxMaster=TaxMaster(taxMasterID=TAXM-1304473371, jpin=JPIN-1304472092, hsnCode=21011120, sinTax=null, cess=null, vatPercentage=5.5, gstPercentage=18.0, gstComponentShare={CGST=50.0, SGST=50.0}, validityPeriodStartDate=null, validityPeriodEndDate=null), vat=5.5, sellingPrice=1.75, orderItemAmount=504, shippingCharges=0.14400000000000002, createdUnitQuantity=288, confirmedUnitQuantity=0, underProcessUnitQuantity=0, readyToShipUnitQuantity=0, shippedUnitQuantity=0, inTransitUnitQuantity=0, deliveredUnitQuantity=0, returnedUnitQuantity=0, cancelledUnitQuantity=0, returnRequestedQuantity=0, returnToOriginQuantity=0, rtsTime=null, orderItemStatus=Created, orderItemStatusRemark=null, invoiceNumbers=null, orderPlacedTimestamp=1567666853965, tradeOffer=null, offers=[], jtOfferBOList=null, appliedJtOfferIds=[], cartItemID=CRTITM-1307885431, invoicedQuantity=0, invoiceItemIdToQtyMap=null, replaceeOrderItemId=BLTORDITM-1318492004, isReplacement=false, returnedOrderItemId=null, statusUpdateHistory=null, mpCommission=MpCommissionBO(entityId=SP-1388879426, entityType=SELLER_PRODUCT, mpCommissionType=PERCENTAGE, value=0.1), rmcTaxPercentage=null)
Exception : null
StackTrace :

-- 
You received this message because you are subscribed to the Google Groups "tech" group.
To unsubscribe from this group and stop receiving emails from it, send an email to tech+unsubscribe@jumbotail.com.
To view this discussion on the web visit https://groups.google.com/a/jumbotail.com/d/msgid/tech/0102016cffe1d18e-a8a50221-d496-4a33-97ea-a179c35c1395-000000%40eu-west-1.amazonses.com.
Look into the failure. System Will NOT Retry Anymore !!!
Request : BoltOrderItem(orderItemId=BLTORDITM-1318488782, orderId=BLTORD-1311815635, buyerId=JC-1202512451, sellerId=ORGPROF-1304473270, listingId=LST-1209584081, boltOrderType=MARKETPLACE, rpoReasonCode=null, quantity=20, mrp=91, taxMaster=TaxMaster(taxMasterID=TAXM-1304473248, jpin=JPIN-1304473143, hsnCode=15119090, sinTax=null, cess=null, vatPercentage=5.5, gstPercentage=5.0, gstComponentShare={CGST=50.0, SGST=50.0}, validityPeriodStartDate=null, validityPeriodEndDate=null), vat=5.5, sellingPrice=68, orderItemAmount=1360, shippingCharges=4.6000000000000005, createdUnitQuantity=20, confirmedUnitQuantity=0, underProcessUnitQuantity=0, readyToShipUnitQuantity=0, shippedUnitQuantity=0, inTransitUnitQuantity=0, deliveredUnitQuantity=0, returnedUnitQuantity=0, cancelledUnitQuantity=0, returnRequestedQuantity=0, returnToOriginQuantity=0, rtsTime=null, orderItemStatus=Created, orderItemStatusRemark=null, invoiceNumbers=null, orderPlacedTimestamp=1567660938708, tradeOffer=OfferBO(offerId=BOLTOFR-1190304707, offerTypeString=Cash Discount, active=true, discountDetailsList=[]), offers=[], jtOfferBOList=null, appliedJtOfferIds=[], cartItemID=CRTITM-1307880835, invoicedQuantity=0, invoiceItemIdToQtyMap=null, replaceeOrderItemId=BLTORDITM-1318488783, isReplacement=false, returnedOrderItemId=null, statusUpdateHistory=null, mpCommission=MpCommissionBO(entityId=SP-1388875162, entityType=SELLER_PRODUCT, mpCommissionType=PERCENTAGE, value=1.3), rmcTaxPercentage=null)
Exception : null
StackTrace :

"""
failStrItem = failStr.split(',')
str1 = ''
failListItem = []



buyerId = re.compile("buyerId=.*")
quantity = re.compile("quantity=.*")
listingId = re.compile("listingId=.*")
sellingPrice = re.compile("sellingPrice=.*")
shippingCharges = re.compile("shippingCharges=.*")
orderItemAmount = re.compile("orderItemAmount=.*")
orderItemId = re.compile("orderItemId=.*")
jpin = re.compile("jpin=.*")


for i in failStrItem:
    if re.findall(buyerId,i):#A
        str1 = (re.findall(buyerId, i))[0]
        failListItem.append(str1)
    if re.findall(listingId,i):#B
        str1 = (re.findall(listingId, i))[0]
        failListItem.append(str1)
    if re.findall(quantity,i):#C
        str1 = (re.findall(quantity, i))[0]
        failListItem.append(str1)
    if re.findall(sellingPrice,i):#D
        str1 = (re.findall(sellingPrice, i))[0]
        failListItem.append(str1)
    if re.findall(shippingCharges,i):#E
        str1 = (re.findall(shippingCharges, i))[0]
        failListItem.append(str1)
    if re.findall(orderItemAmount,i):#F
        str1 = (re.findall(orderItemAmount, i))[0]
        failListItem.append(str1)
    if re.findall(orderItemId,i):#I
        str1 = (re.findall(orderItemId, i))[0]
        failListItem.append(str1)
    if re.findall(jpin,i):#J
        str1 = (re.findall(jpin,i))[0]
        failListItem.append(str1)

print((failListItem[0].split('=')[0]))

buyerId = []
listingId = []
quantity = []
sellingPrice = []
shippingCharges = []
orderItemAmount = []
orderItemId = []
jpin = []

for i in failListItem:
    if i.split('=')[0] == 'buyerId':
        buyerId.append(i.split('=')[1])
    if i.split('=')[0] == 'listingId':
        listingId.append(i.split('=')[1])
    if i.split('=')[0] == 'quantity':
        quantity.append(i.split('=')[1])
    if i.split('=')[0] == 'sellingPrice':
        sellingPrice.append(i.split('=')[1])
    if i.split('=')[0] == 'shippingCharges':
        shippingCharges.append(i.split('=')[1])
    if i.split('=')[0] == 'orderItemAmount':
        orderItemAmount.append(i.split('=')[1])
    if i.split('=')[0] == 'orderItemId':
        orderItemId.append(i.split('=')[1])
    if i.split('=')[0] == 'jpin':
        jpin.append(i.split('=')[1])

res = []
bId = [b for b in buyerId]
lId = [l for l in listingId]
qty = [q for q in quantity]
slp = [s for s in sellingPrice]
spc = [c for c in shippingCharges]
oia = [o for o in orderItemAmount]
oid = [d for d in orderItemId]
jpin = [j for j in jpin]

for i in range(len(buyerId)):
    res.append([bId[i],lId[i],qty[i],slp[i],spc[i],oia[i],"","TRUE",oid[i],jpin[i]])
    
row = 1
column = 1

with open("orders_now_2.csv","w") as out_file: #specify folder
    csv_write = csv.writer(out_file)
    csv_write.writerow(['CustomerId','ListingId','Requested Quantity','Final Price Per Unit','Shipping Charges','Order Item Amount','Product Title','Is Replacement','For Entity ID','Group Request Id','Request Type','Request Status','Replaced Entity ID','System Calculate Price','jpin'])
    for i in res:
        csv_write.writerow(i)