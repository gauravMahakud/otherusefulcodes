#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 06:31:24 2019

@author: gaurav Mahakud
@description: this makes equal batches of the any 'One column data' from a CSV.
"""

import csv

all_entries = []
limit = 9000
with open ("/Users/jumbotail/Documents/query_result.csv","r") as input_csv:
    lines = csv.reader(input_csv,delimiter = ",")
    for i in lines:
        all_entries.append(i)

batches = []
def process_batch(l):
        print("Total Rows to be updated",len(l))
        rest = len(l) % limit
        
        div = len(l)-rest
        print(type(div),div)
        print(type(rest),rest)
        create_batch(l,div)
        batches.append(l[0:rest])
        print(len(batches))

        
def create_batch(l,div):
        sbatch = []
        if div % limit == 0:
            if len(l)>0:
                #print("in here")
                for i in range(limit):
                    sbatch.append(l.pop())
                batches.append(sbatch)
                sbatch = []
                #print(div)
                div = div - limit
                #print("breaking down: ",div)
                if div:
                    create_batch(l,div)
                else:
                    print("done")
            else:
                print("batch Prepared")

        else:
            print("port")

        
process_batch(all_entries)
print(len(batches))

for i in range(len(batches)):
    print(len(batches[i]))
    if len(batches[i])==limit:
        print(batches[i][limit-1])
    else:
        print(batches[i][len(batches[i])-1])

for b in range(len(batches)):
    with open(f"/Users/jumbotail/Documents/temp/processed_batch{b}.csv","w") as csv_out:
        csv_writer = csv.writer(csv_out,delimiter = ",")
        for i in batches[b]:
            csv_writer.writerow(i)