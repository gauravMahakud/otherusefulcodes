#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  3 13:04:30 2019

@author: gaurav Mahakud


"""

import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import pprint


pp = pprint.PrettyPrinter(indent = 4)

scanList = [
"BLTORDITM-1313167419"
]
empty = []
# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

dynamodb = boto3.resource("dynamodb")

orderId = 'BLTORD-1313143960'#input("enter OrderId: ")
orderItem = 'BLTORDITM-1318331780' #input("enter OrderItemId: ")
#listing_id = input("Enter Listing_id: ")


boltOrderItem = dynamodb.Table("BoltOrderItem")
fulfilmentItem = dynamodb.Table("FulfilmentItem")
listing = dynamodb.Table("Listing")
boltLot = dynamodb.Table("BoltLot")
lotInventory = dynamodb.Table("LotInventory")
listingInventory = dynamodb.Table("ListingInventory")

orderItemRes = []
fulfilRes = []
listingRes = []
lotRes = []
lotInvRes = []
listInvRes = []


try: 
	response = boltOrderItem.query(
			IndexName = 'orderId',
			KeyConditionExpression = Key('orderId').eq(orderId)
			)
except ClientError as e:
    print(e.response['Error']['Message'])
else: 
	orderItemRes.append(response['Items'])
	print("---boltOrderItem GetItem succeeded----\n")

#pp.pprint(orderItemRes)
orderItemDict={}
for i in range(len(orderItemRes[0])):
    orderItemDict.update({'orderItemId':orderItemRes[0][i]['orderItemId']})    
    orderItemDict.update({'cartItemID':orderItemRes[0][i]['cartItemID']})
    orderItemDict.update({'buyerId': orderItemRes[0][i]['buyerId']})
    orderItemDict.update({'boltOrderType': orderItemRes[0][i]['boltOrderType']})
    orderItemDict.update({'listingId': orderItemRes[0][i]['listingId']})
    print('orderItemId: ',orderItemRes[0][i]['orderItemId'])
    print('cartItemID: ',orderItemRes[0][i]['cartItemID'])
    print('buyerId: ', orderItemRes[0][i]['buyerId'])
    print('boltOrderType: ', orderItemRes[0][i]['boltOrderType'])
    print('listingId: ', orderItemRes[0][i]['listingId'])
    print('qty: ', orderItemRes[0][i]['quantity'])
#pp.pprint(response['Items'])
        
print(f"------------{len(orderItemRes[0])}---------------\n\n")


fulfilDict = {}
try:
    response = fulfilmentItem.query(
                IndexName = 'orderItemId-type',
                KeyConditionExpression=Key('orderItemId').eq(orderItem)
                )
except ClientError as e:
    print(e.response['Error']['Message'])
else:
    fulfilRes.append(response['Items'])
    pp.pprint(response['Items'])
    print("---fulfilment GetItem succeeded---\n")  
    
for i in range(len(fulfilRes[0])):
    fulfilDict.update({'createdTime': fulfilRes[0][i]['createdTime']})
    fulfilDict.update({'fulfilmentItemId': fulfilRes[0][i]['fulfilmentItemId']})
    fulfilDict.update({'fulfilmentStateCount': fulfilRes[0][i]['fulfilmentStateCount']})
    fulfilDict.update({'lastUpdatedBy': fulfilRes[0][i]['lastUpdatedBy']})
    fulfilDict.update({'lastUpdatedTime': fulfilRes[0][i]['lastUpdatedTime']})
    fulfilDict.update({'lotId': fulfilRes[0][i]['lotId']})
    fulfilDict.update({'orderItemId': fulfilRes[0][i]['orderItemId']})
    fulfilDict.update({'ownerId': fulfilRes[0][i]['ownerId']})
    fulfilDict.update({'quantity': fulfilRes[0][i]['quantity']})
    fulfilDict.update({'salesChannel': fulfilRes[0][i]['salesChannel']})
    fulfilDict.update({'type': fulfilRes[0][i]['type']})
    fulfilDict.update({'version': fulfilRes[0][i]['version']})
    
    

print(f"------------{len(response['Items'])}---------------\n\n")

lotDict = {}
for i in range(len(fulfilRes[0])):
    blotId = fulfilRes[0][i]['lotId']
    print(blotId)
    try: 
        response = boltLot.query( 
                 #IndexName = 'ownerId',
                 KeyConditionExpression = Key('bLotId').eq(blotId)
                 )
    except ClientError as e:
        print(e.response['Error']['Message'])
        
    else:
        pp.pprint(response['Items'])
        lotRes.append(response['Items'])
        print("listing GetItem succeeded")

for i in range(len(lotRes[0])):
    lotDict.update({'bLotId': lotRes[0][i]['bLotId']})
    lotDict.update({'channelId':lotRes[0][i]['channelId']})
    lotDict.update({'lastUpdatedBy': lotRes[0][i]['lastUpdatedBy']})
    lotDict.update({'lastUpdatedTime': lotRes[0][i]['lastUpdatedTime']})
    lotDict.update({'listingId': lotRes[0][i]['listingId']})
    lotDict.update({'lotType': lotRes[0][i]['lotType']})
    lotDict.update({'maxOrderQuantity': lotRes[0][i]['maxOrderQuantity']})
    lotDict.update({'minOrderQuantity': lotRes[0][i]['minOrderQuantity']})
    lotDict.update({'mrp': lotRes[0][i]['mrp']})
    lotDict.update({'orderMultiples': lotRes[0][i]['orderMultiples']})
    if lotRes[0][i]['lotType'] == "JIT":
        lotDict.update({'purchaselistingId': lotRes[0][i]['purchaselistingId']})    
    lotDict.update({'otherLdas': lotRes[0][i]['otherLdas']})
    lotDict.update({'ownerId': lotRes[0][i]['ownerId']})
    lotDict.update({'requestId': lotRes[0][i]['requestId']})
    lotDict.update({'sellingPrice': lotRes[0][i]['sellingPrice']})
    lotDict.update({'spid': lotRes[0][i]['spid']})
    lotDict.update({'status': lotRes[0][i]['status']})
    lotDict.update({'vendorId': lotRes[0][i]['vendorId']})
    lotDict.update({'version': lotRes[0][i]['version']})
    

print(f"------------{len(response['Items'])}---------------\n\n")

lotInvDict = {}
for i in range(len(fulfilRes[0])):
    blotId = fulfilRes[0][i]['lotId']
    print(blotId)
    try: 
        response = lotInventory.query( 
                 #IndexName = 'ownerId',
                 KeyConditionExpression = Key('bLotId').eq(blotId)
                 )
    except ClientError as e:
        print(e.response['Error']['Message'])
        
    else:
        lotInvRes.append(response['Items'])
        pp.pprint(response['Items'])
        print("---LotInventory GetItem succeeded---")
        
for i in range(len(lotInvRes[0])):
    lotInvDict.update({'bLotId':lotInvRes[0][i] ['bLotId']})
    lotInvDict.update({'blockedFromSale':lotInvRes[0][i] ['blockedFromSale']})
    lotInvDict.update({'lastUpdatedBy':lotInvRes[0][i] ['lastUpdatedBy']})
    lotInvDict.update({'lastUpdatedTime':lotInvRes[0][i] ['lastUpdatedTime']})
    lotInvDict.update({'lotType':lotInvRes[0][i] ['lotType']})
    lotInvDict.update({'ownerId':lotInvRes[0][i] ['ownerId']})
    lotInvDict.update({'requestId':lotInvRes[0][i] ['requestId']})
    lotInvDict.update({'status':lotInvRes[0][i] ['status']})
    lotInvDict.update({'stockMap':lotInvRes[0][i] ['stockMap']})
    lotInvDict.update({'totalReceived':lotInvRes[0][i] ['totalReceived']})
    lotInvDict.update({'totalSold':lotInvRes[0][i] ['totalSold']})
    lotInvDict.update({'version':lotInvRes[0][i] ['version']})



   