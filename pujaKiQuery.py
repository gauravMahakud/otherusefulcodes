#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 18:11:15 2019

@author: jumbotail
"""
import pymysql
import datetime
rds_host = "prod-read-replica.ce6lasmdz6oo.ap-southeast-1.rds.amazonaws.com"
name = "__"
password = "__"
db_name = "jt_jobs_prod"
port = 4008
conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, port=port,
                                   autocommit=True)



def run_this(conn):
    
    cur = conn.cursor()
    res1 = cur.execute('''
insert into jt_appsheet.last_inwarded_inventory_item
select distinct natural_id
from jt_space_prod.inventory_item ii
Left join jt_appsheet.last_inwarded_inventory_item ii2 on ii2.inventory_item_id=ii.natural_id
where created_time>=unix_timestamp(current_timestamp) * 1000 - 3600000*.25 and state='INWARDED' and status!='CANCELLED' and ii2.inventory_item_id is null;''')


    res2 = cur.execute('''insert into jt_appsheet.inward_audit
select distinct concat(unix_timestamp(current_timestamp) * 1000,inw.origin_process_ref_id ) as identifier,
inw.product_id, 
inw.listing_id,
inw.origin_process_ref_id as new_inwarding_id,
inw.inwarded_qty,
inw.material_inwarding_time,
st.origin_process_ref_id, 
st.available_units,unix_timestamp(current_timestamp) * 1000  as update_timestamp
from(select product_id,listing_id,origin_process_ref_id, sum(initial_qty) as inwarded_qty,min(created_time) as material_inwarding_time
from jt_space_prod.inventory_item ii
join jt_appsheet.last_inwarded_inventory_item iif on iif.inventory_item_id=ii.natural_id
where created_time>=unix_timestamp(current_timestamp) * 1000 - 3600000*.25 and state='INWARDED' and status!='CANCELLED'
group by 1,2,3) inw
join (select product_id,listing_id,origin_process_ref_id,sum(left_qty) as available_units
from jt_space_prod.inventory_item
where state in ('INWARDED','SELLABLE') and status in ('ACTIVE','ONHOLD')
group by 1,2,3) st on st.listing_id=inw.listing_id;
''')
    return res1,res2

res1,res2 = run_this(conn)
with open("/Users/jumbotail/testing/testres.txt","a") as ouput:
    ouput.write(f"\nat:{datetime.datetime.now()} query 1 affected:{res1} rows and query 2 affected:{res2} rows")
    


