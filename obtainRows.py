import csv
import re
import datetime
import gspread
import numpy as np
from oauth2client.service_account import ServiceAccountCredentials


class AuthAndCred:
    match = []
    uniq_match = []
    client = None
    def __init__(self):
        scope = ['https://www.googleapis.com/auth/drive']
        try:
            #creds = ServiceAccountCredentials.from_json_keyfile_name('/home/gauravmhkd/automated_queries/OrderFailureScript/logUpload-e8f2a523a7e5.json', scope)
            creds = ServiceAccountCredentials.from_json_keyfile_name('/Users/jumbotail/Downloads/logUpload-e8f2a523a7e5.json', scope)
            print("auth success")
        except:
            print("auth failed, Could not find the credential json or found expired key")

        try:
            self.client = gspread.authorize(creds)
            print("gspread success")
        except:
            print("gspread is not recognising the drive")

        self.input_sheet = self.client.open("failed_orders").sheet1
        self.output_sheet = self.client.open("output_sheet").sheet1

    def get_records(self):
        list_of_hashes = self.input_sheet.get_all_records()
        hash_list = str(list_of_hashes).split("\\r")
        return hash_list

    def obtain_list(self, hash_list):
        pattern = re.compile(r"Request : .*")
        for string in hash_list:
            if re.match(pattern, string) is not None:
                self.match.append(re.match(pattern, string).group())

        for item in self.match:
            if item not in self.uniq_match:
                self.uniq_match.append(item)

        #print(self.uniq_match)
        return self.uniq_match

    def write_to_google_sheet(self):
        csv_list = []
        with open("orders_now.csv", "r") as input_csv:
            read_csv_list = csv.reader(input_csv)
            for row in read_csv_list:
                csv_list.append(row)

        print(len(csv_list))
        print(datetime.datetime.now())
        list2 = np.array(csv_list)
        cells = []
        for row_num, row in enumerate(list2):
            for col_num, cell in enumerate(row):
                cells.append(gspread.Cell(row_num + 1, col_num + 1, list2[row_num][col_num]))
        self.output_sheet.update_cells(cells)


def main():
    sheet_obj = AuthAndCred()
    obtaining_obj = sheet_obj.get_records()
    rows = sheet_obj.obtain_list(obtaining_obj)

    fail_str = ''
    for row in rows:
        fail_str += row

    fail_str_item = fail_str.split(',')
    str1 = ''
    fail_item_list = []

    buyerId = re.compile("buyerId=.*")
    quantity = re.compile("quantity=.*")
    listingId = re.compile("listingId=.*")
    sellingPrice = re.compile("sellingPrice=.*")
    shippingCharges = re.compile("shippingCharges=.*")
    orderItemAmount = re.compile("orderItemAmount=.*")
    orderItemId = re.compile("orderItemId=.*")
    jin = re.compile("jpin=.*")

    for i in fail_str_item:
        if re.findall(buyerId, i):  # A
            str1 = (re.findall(buyerId, i))[0]
            fail_item_list.append(str1)
        if re.findall(listingId, i):  # B
            str1 = (re.findall(listingId, i))[0]
            fail_item_list.append(str1)
        if re.findall(quantity, i):  # C
            str1 = (re.findall(quantity, i))[0]
            fail_item_list.append(str1)
        if re.findall(sellingPrice, i):  # D
            str1 = (re.findall(sellingPrice, i))[0]
            fail_item_list.append(str1)
        if re.findall(shippingCharges, i):  # E
            str1 = (re.findall(shippingCharges, i))[0]
            fail_item_list.append(str1)
        if re.findall(orderItemAmount, i):  # F
            str1 = (re.findall(orderItemAmount, i))[0]
            fail_item_list.append(str1)
        if re.findall(orderItemId, i):  # I
            str1 = (re.findall(orderItemId, i))[0]
            fail_item_list.append(str1)
        if re.findall(jin,i): 
            str1 = (re.findall(jin,i))[0] 
            fail_item_list.append(str1)
            
    print((fail_item_list[0]).split("=")[0])

    buyerId = []
    listingId = []
    quantity = []
    sellingPrice = []
    shippingCharges = []
    orderItemAmount = []
    orderItemId = []
    jpin = []

    for i in fail_item_list:
        if i.split('=')[0] == 'buyerId':
            buyerId.append(i.split('=')[1])
        if i.split('=')[0] == 'listingId':
            listingId.append(i.split('=')[1])
        if i.split('=')[0] == 'quantity':
            quantity.append(i.split('=')[1])
        if i.split('=')[0] == 'sellingPrice':
            sellingPrice.append(i.split('=')[1])
        if i.split('=')[0] == 'shippingCharges':
            shippingCharges.append(i.split('=')[1])
        if i.split('=')[0] == 'orderItemAmount':
            orderItemAmount.append(i.split('=')[1])
        if i.split('=')[0] == 'orderItemId':
            orderItemId.append(i.split('=')[1])
        if i.split('=')[0] == 'jpin':
            jpin.append(i.split('=')[1])	

    res = []
    bId = [b for b in buyerId]
    lId = [l for l in listingId]
    qty = [q for q in quantity]
    slp = [s for s in sellingPrice]
    spc = [c for c in shippingCharges]
    oia = [o for o in orderItemAmount]
    oid = [d for d in orderItemId]
    jin = [j for j in jpin]

    for i in range(len(buyerId)):
        print([bId[i], lId[i], qty[i], slp[i], spc[i], oia[i], "", "TRUE", oid[i],jin[i]])
        res.append([bId[i], lId[i], qty[i], slp[i], spc[i], oia[i], "", "TRUE", oid[i],jin[i]])
    try:
        with open("orders_now.csv", "w") as out_file:
            csv_write = csv.writer(out_file)
            csv_write.writerow(['CustomerId', 'ListingId', 'Requested Quantity', 'Final Price Per Unit', 'Shipping','Charges',
                            'Order Item Amount', 'Product Title', 'Is Replacement', 'For Entity ID', 'Group Request Id',
                            'Request Type', 'Request Status', 'Replaced Entity ID', 'System Calculate Price'])
            for i in res:
                csv_write.writerow(i)
        print("file orders_now.csv created")
    except:
        print("error while writing to file: orders_now")

    try:
        sheet_obj.write_to_google_sheet()
        print("google sheet written with order items")
    except:
        print("error while writing to google sheet")


if __name__ == '__main__':
    main()
